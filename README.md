## htl-homework

cp .env.example .env <br>
composer install <br>
php artisan key:generate <br>
php artisan migrate --seed <br>
npm install <br>
npm run dev <br>

LOGIN CREDENTIALS <br>
Email: `admin@test.com` <br>
Password: `password` <br>
