import OrdersList from './pages/orders/List';
import OrdersCreate from './pages/orders/Create';
import OrdersDetail from './pages/orders/Detail';

export const routes = [
    {
        path: '/orders',
        name: 'OrdersList',
        component: OrdersList
    },
    {
        path: '/orders/create',
        name: 'OrdersCreate',
        component: OrdersCreate
    },
    {
        path: '/orders/:id',
        name: 'OrdersDetail',
        component: OrdersDetail
    }
];
