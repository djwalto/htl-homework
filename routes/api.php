<?php

use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\OrderDataController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/orders/keys', [OrderDataController::class, 'keys']);
Route::get('/orders/vehicles', [OrderDataController::class, 'vehicles']);
Route::get('/orders/{vehicle}/keys', [OrderDataController::class, 'vehicle_keys']);
Route::get('/orders/technicians', [OrderDataController::class, 'technicians']);
Route::get('/orders/{order}', [OrderDataController::class, 'orders']);

Route::apiResource('orders', OrderController::class);
