<?php

use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/orders', [OrderController::class, 'index'])->name('orders.index');

Route::post('users/{user}/change-password', [ChangePasswordController::class, 'change_password'])->name('users.changePassword');
