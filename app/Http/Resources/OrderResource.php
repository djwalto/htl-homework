<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array|Arrayable|JsonSerializable
     */

    public function toArray($request)
    {
        // Returning the key, vehicle, and technician relationships with order
        return [
            'id' => $this->id,
            'key_id' => $this->key->id,
            'key_name' => $this->key->name,
            'key_description' => $this->key->description,
            'key_price' => $this->key->price,
            'vehicle_id' => $this->vehicle->id,
            'vehicle_year' => $this->vehicle->year,
            'vehicle_make' => $this->vehicle->make,
            'vehicle_model' => $this->vehicle->model,
            'vehicle_vin' => $this->vehicle->vin,
            'technician_first_name' => $this->technician->first_name,
            'technician_last_name' => $this->technician->last_name,
            'technician_truck_number' => $this->technician->truck_number,
        ];
    }
}
