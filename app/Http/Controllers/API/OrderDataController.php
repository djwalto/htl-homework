<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Key;
use App\Models\Technician;
use App\Models\Vehicle;

class OrderDataController extends Controller
{
    public function vehicles()
    {
        // Fetches all vehicles from db and returns
        $vehicles = Vehicle::all();

        return response()->json($vehicles);
    }

    public function keys()
    {
        // Fetches all keys from db and returns
        $keys = Key::all();

        return response()->json($keys);
    }

    public function vehicle_keys(Vehicle $vehicle)
    {
        // Fetches all keys specific to this vehicle from db and returns vehicle with keys
        return response()->json($vehicle->keys);
    }

    public function technicians()
    {
        // Fetches all technicians from db and returns
        $technicians = Technician::all();

        return response()->json($technicians);
    }
}
