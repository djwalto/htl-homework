<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderStoreRequest;
use App\Http\Resources\OrderResource;
use App\Http\Resources\OrderSingleResource;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        // Display paginated orders by newest first
        $orders = Order::orderBy('id', 'DESC')->paginate(5);

        // List orders by keys, vehicles, or technician
        if ($request->key_id) {
            $orders = Order::where('key_id', $request->key_id)->orderBy('id', 'DESC')->get();
        }
        if ($request->vehicle_id) {
            $orders = Order::where('vehicle_id', $request->vehicle_id)->orderBy('id', 'DESC')->get();
        }
        if ($request->technician_id) {
            $orders = Order::where('technician_id', $request->technician_id)->orderBy('id', 'DESC')->get();
        }

        // Return array of orders with relationships included
        return OrderResource::collection($orders);
    }

    public
    function store(OrderStoreRequest $request)
    {
        // $request is validated through OrderStoreRequest before order is created
        $order = Order::create($request->validated());

        // Returning new $order
        return response()->json($order);
    }

    public
    function show(Order $order)
    {
        // Return single order with certain relationship included
        return new OrderSingleResource($order);
    }

    public
    function update(OrderStoreRequest $request, Order $order)
    {
        // $request is validated through OrderStoreRequest before $order is updated
        $order->update($request->validated());

        // Return message for success
        return response()->json('Order Updated Successfully');
    }

    public
    function destroy(Order $order)
    {
        // Order is removed from db
        $order->delete();

        // Return message for success
        return response()->json('Order Deleted Successfully');
    }
}
