<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    // Validation rules to be checked before order is created or updated
    public function rules()
    {
        return [
            'key_id' => ['required'],
            'vehicle_id' => ['required'],
            'technician_id' => ['required'],
        ];
    }
}
