<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeyVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    // Pivot table for Keys and Vehicles tables
    public function up()
    {
        Schema::create('key_vehicle', function (Blueprint $table) {
            $table->id();
            $table->foreignId('key_id');
            $table->foreignId('vehicle_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('key_vehicle');
    }
}
