<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            KeySeeder::class,
            VehicleSeeder::class,
            TechnicianSeeder::class,
            OrderSeeder::class,
            KeyVehicleSeeder::class,
            UserSeeder::class
        ]);
    }
}
