<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keys = [
            [
                'name' => 'Item A',
                'description' => 'This is Item A',
                'price' => '1.00',
            ],
            [
                'name' => 'Item B',
                'description' => 'This is Item B',
                'price' => '2.00',
            ],
            [
                'name' => 'Item C',
                'description' => 'This is Item C',
                'price' => '3.00',
            ],
            [
                'name' => 'Item D',
                'description' => 'This is Item D',
                'price' => '4.00',
            ],
            [
                'name' => 'Item E',
                'description' => 'This is Item E',
                'price' => '5.00',
            ],
            [
                'name' => 'Item F',
                'description' => 'This is Item F',
                'price' => '6.00',
            ],
            [
                'name' => 'Item G',
                'description' => 'This is Item G',
                'price' => '7.00',
            ],
            [
                'name' => 'Item H',
                'description' => 'This is Item H',
                'price' => '8.00',
            ],
            [
                'name' => 'Item I',
                'description' => 'This is Item I',
                'price' => '9.00',
            ],
            [
                'name' => 'Item J',
                'description' => 'This is Item J',
                'price' => '10.00',
            ],
            [
                'name' => 'Item K',
                'description' => 'This is Item K',
                'price' => '11.00',
            ],
            [
                'name' => 'Item L',
                'description' => 'This is Item L',
                'price' => '12.00',
            ],
            [
                'name' => 'Item M',
                'description' => 'This is Item M',
                'price' => '13.00',
            ],
            [
                'name' => 'Item N',
                'description' => 'This is Item N',
                'price' => '14.00',
            ],
            [
                'name' => 'Item O',
                'description' => 'This is Item O',
                'price' => '15.00',
            ],
            [
                'name' => 'Item P',
                'description' => 'This is Item P',
                'price' => '16.00',
            ],
            [
                'name' => 'Item Q',
                'description' => 'This is Item Q',
                'price' => '17.00',
            ],
            [
                'name' => 'Item R',
                'description' => 'This is Item R',
                'price' => '18.00',
            ],
            [
                'name' => 'Item S',
                'description' => 'This is Item S',
                'price' => '19.00',
            ],
            [
                'name' => 'Item T',
                'description' => 'This is Item T',
                'price' => '20.00',
            ]
        ];

        DB::table("keys")->insert($keys);
    }
}
