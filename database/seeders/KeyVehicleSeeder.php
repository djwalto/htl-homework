<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KeyVehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $key_vehicle = [
            [
                'key_id' => '1',
                'vehicle_id' => '1',
            ],
            [
                'key_id' => '2',
                'vehicle_id' => '2',
            ],
            [
                'key_id' => '4',
                'vehicle_id' => '4',
            ],
            [
                'key_id' => '5',
                'vehicle_id' => '3',
            ],
            [
                'key_id' => '11',
                'vehicle_id' => '6',
            ],
            [
                'key_id' => '15',
                'vehicle_id' => '18',
            ],
            [
                'key_id' => '14',
                'vehicle_id' => '3',
            ],
            [
                'key_id' => '13',
                'vehicle_id' => '8',
            ],
            [
                'key_id' => '16',
                'vehicle_id' => '9',
            ],
            [
                'key_id' => '17',
                'vehicle_id' => '11',
            ],
            [
                'key_id' => '7',
                'vehicle_id' => '13',
            ],
            [
                'key_id' => '4',
                'vehicle_id' => '15',
            ],
            [
                'key_id' => '9',
                'vehicle_id' => '14',
            ],
            [
                'key_id' => '4',
                'vehicle_id' => '6',
            ],
            [
                'key_id' => '8',
                'vehicle_id' => '6',
            ],
            [
                'key_id' => '3',
                'vehicle_id' => '8',
            ],
            [
                'key_id' => '9',
                'vehicle_id' => '18',
            ],
            [
                'key_id' => '9',
                'vehicle_id' => '19',
            ],
            [
                'key_id' => '7',
                'vehicle_id' => '16',
            ],
            [
                'key_id' => '4',
                'vehicle_id' => '2',
            ]
        ];

        DB::table("key_vehicle")->insert($key_vehicle);
    }
}
