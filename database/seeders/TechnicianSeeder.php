<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TechnicianSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $technicians = [
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 1,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 2,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 3,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 4,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 5,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 6,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 7,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 8,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 9,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 10,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 11,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 12,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 13,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 14,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 15,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 16,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 17,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 18,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 19,
            ],
            [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'truck_number' => 20,
            ]
        ];

        DB::table("technicians")->insert($technicians);
    }
}
