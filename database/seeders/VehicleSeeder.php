<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vehicles = [
            [
                'year' => rand(2000, 2022),
                'make' => 'ACURA',
                'model' => 'INTEGRA',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'BUICK',
                'model' => 'REGAL',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'CHEVROLET',
                'model' => 'TAHOE',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'DODGE',
                'model' => 'RAM',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'FORD',
                'model' => 'MUSTANG',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'GEO',
                'model' => 'METRO',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'HONDA',
                'model' => 'ACCORD',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'INFINITI',
                'model' => 'G37',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'JAGUAR',
                'model' => 'X-TYPE',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'KIA',
                'model' => 'STINGER',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'KIA',
                'model' => 'TELLURIDE',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'LAMBORGHINI',
                'model' => 'DIABLO',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'MERCURY',
                'model' => 'COUGAR',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'NISSAN',
                'model' => 'GTR',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'OLDSMOBILE',
                'model' => 'CUTLASS',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'PORSCHE',
                'model' => 'CAYENNE',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'RAM',
                'model' => '1500',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'SCION',
                'model' => 'TC',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'TESLA',
                'model' => 'MODEL S',
                'vin' => $this->createVIN(),
            ],
            [
                'year' => rand(2000, 2022),
                'make' => 'VOLKSWAGEN',
                'model' => 'BEETLE',
                'vin' => $this->createVIN(),
            ],
        ];

        DB::table("vehicles")->insert($vehicles);
    }

    protected function createVIN(): string
    {
        $characters = '0123456789abcdefghjklmnpqrstuvwxyz';
        $vin = substr(str_shuffle(str_repeat($characters, 17)), 0, 17);

        return strtoupper($vin);
    }
}
